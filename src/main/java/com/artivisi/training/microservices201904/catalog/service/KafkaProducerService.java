package com.artivisi.training.microservices201904.catalog.service;

import com.artivisi.training.microservices201904.catalog.entity.Produk;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerService.class);

    @Autowired private ObjectMapper objectMapper;
    @Autowired private KafkaTemplate<String, String> kafkaTemplate;
    @Value("${topic.notification}") private String topicNotifikasi;

    public void kirimNotifikasiProdukBaru(Produk produk) {
        try {
            LOGGER.debug("Mengirim notifikasi produk baru : {}", produk);
            String msg = objectMapper.writeValueAsString(produk);
            LOGGER.debug("Produk dalam format json : {}", msg);
            kafkaTemplate.send(topicNotifikasi, msg);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
