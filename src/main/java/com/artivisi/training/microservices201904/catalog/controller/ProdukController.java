package com.artivisi.training.microservices201904.catalog.controller;

import com.artivisi.training.microservices201904.catalog.dao.ProdukDao;
import com.artivisi.training.microservices201904.catalog.entity.Produk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.artivisi.training.microservices201904.catalog.service.KafkaProducerService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;


@RestController
public class ProdukController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProdukController.class);

    @Autowired private ProdukDao produkDao;
    @Autowired private KafkaProducerService kafkaProducerService;
    
    
    @GetMapping("/hostinfo")
    public Map<String, Object> hostInfo(HttpServletRequest request) throws UnknownHostException {
        LOGGER.info("Menampilkan data host");
        Map<String, Object> info = new HashMap<>();
        info.put("hostname", InetAddress.getLocalHost().getHostName());
        info.put("address", request.getLocalAddr());
        info.put("port", request.getLocalPort());
        return info;
    }

    
    @GetMapping("/produk/")
    public Page<Produk> dataProduk(Pageable page) {
        LOGGER.info("Menampilkan data produk dari database");
        return produkDao.findAll(page);
    }
    
    @PostMapping("/produk/")
    @ResponseStatus(HttpStatus.CREATED)
    public void simpanProdukBaru(@RequestBody @Valid Produk produk) {
        produkDao.save(produk);
        kafkaProducerService.kirimNotifikasiProdukBaru(produk);
    }


}
