package com.artivisi.training.microservices201904.catalog.dao;

import com.artivisi.training.microservices201904.catalog.entity.Produk;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProdukDao extends PagingAndSortingRepository<Produk, String> {

}
